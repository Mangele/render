#! /usr/bin/python3.6
import os
import time
from os import listdir
from os.path import isfile, join
import glob
mypath_train = "./chair_monitor_bed"
objType = 0
#mypath_train = "../ModelNet10/chair/train103"
#mypath_train = "../ModelNet10/monitor100"
#mypath_train = "../ModelNet10/bed100"

# chair 	1
# monitor 2
# Bed 		3

onlyfiles_train = [f for f in listdir(mypath_train) ]
#onlyfiles_train=glob.glob(mypath_train+"/*.off")	

for offFile in range (0,len(onlyfiles_train)):
	if "chair" in onlyfiles_train[offFile]:
		objType = 1
	elif "monitor" in onlyfiles_train[offFile]:
		objType = 2
	elif "bed" in onlyfiles_train[offFile]:
		objType = 3
	#print(mypath_train+"/"+onlyfiles_train[offFile]+" "+objType+"\n")
	print("./taker {0}/{1} {2}\n".format(mypath_train,onlyfiles_train[offFile],objType))
	os.system("./taker "+mypath_train+"/"+onlyfiles_train[offFile]+" "+str(objType)+"\n")


	grayScaleFiles = [ft for ft in listdir(onlyfiles_train[offFile]) ]
	dirName_tmp = grayScaleFiles[0].split("_")
	dirName = dirName_tmp[0]+"_"+dirName_tmp[1]+"/"
#	print("---------------->\n")
#	print(grayScaleFiles)
#	print("---------------->\n")

	with open(onlyfiles_train[offFile]+".tmp", 'w') as outfile:
		#outfile.write("theta,phi,Image\n")
		for fname in grayScaleFiles:
			with open(dirName+fname) as infile:
				outfile.write(infile.read())
			outfile.write("\n")

	outfile.close() 
	time.sleep(1)

csvFiles=glob.glob('./*.tmp')	
#csvFiles = [ft for ft in listdir("./*.csv") ]
with open("chair_monitor_bed_sphe.csv", 'w') as foutfile:
	foutfile.write("objType,theta,phi,Image\n")
	#foutfile.write("objType,phi,tetha,trinche,Image\n")
	for ffname in csvFiles :
		with open(ffname) as infile:
			foutfile.write(infile.read())
		foutfile.write("\n")
	
foutfile.close() 



