#include <math.h>
#include <GL/glut.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include "processOffFile.h"

#define BODY_WIDTH 3
#define BODY_HEIGHT 10
#define BODY_LENGTH 10
#define ARM_HEIGHT 2
#define ARM_LENGTH 4
#define ARM_WIDTH  2
#define HEAD_RADIUS	4
# define M_PI           3.14159265358979323846

#include <sys/stat.h>
#include <sys/types.h>
#include <libgen.h>
#include <errno.h> 

typedef struct cylCoor{
	float r;
	float beta;
	float z;
}t_cylCoor;

typedef struct SpheCoor{
	float theta;
	float phi;
	float roo;
}t_spheCorr;

typedef struct eulerRot {
	
	int phi;
	int tetha;
	int trinche;

}t_eulerRot;

t_eulerRot euler;

//euler.phi = 0;
//euler.tetha = 0;
//euler.trinche = 0;
int alfa = 0;
int betha = 0;
int trinche = 0;


t_spheCorr sphe;
t_cylCoor cyl;
t_off *off;
char *offFileName=NULL;
char *dir=NULL;

void display(void);
void draw_axes(void);
void vertexByPos( int pos );
//void mouse(int button, int state, int x, int y) ;
void mouse();
static void create_ppm(char *prefix, int frame_id, unsigned int width, unsigned int height,
        unsigned int color_max, unsigned int pixel_nbytes, GLubyte *pixels);
static void csv_gray_scale_pgm(char *prefix, int frame_id, unsigned int width, unsigned int height,
        unsigned int color_max, unsigned int pixel_nbytes, GLubyte *pixels);
static void create_pgm(char *prefix, int frame_id, unsigned int width, unsigned int height,
        unsigned int color_max, unsigned int pixel_nbytes, GLubyte *pixels);
t_cylCoor coordinatates( int step );
t_spheCorr spheCoor(int step );

void eulerRot( int step );

void inicializar() ;
//t_axis max;
//t_axis min;
int objType=0;
float maxAxis=0;
int ms = 20;
GLuint window;
void screen_reshape(GLuint sub_width, GLuint sub_height);

GLuint sub_width = 256, sub_height = 256;
void redisplay_all(void);
static GLubyte *pixels = NULL;
static const GLenum FORMAT = GL_RGBA;
static const GLuint FORMAT_NBYTES = 4;
static const unsigned int HEIGHT = 500;
static const unsigned int WIDTH = 500;
static unsigned int nscreenshots = 0;
static unsigned int time;

static double angle = 0;
static double angle_speed = 45;


void usage( char *bin );
static void deinit(void)  {
    free(pixels);          
}                          
  GLfloat lighPosition[] = {2.0,0.0,3.0,0.0};
  GLfloat colorWhite[] = {1.0,1.0,1.0,1.0};
  GLfloat colorDarkGray[] = {0.1,0.1,0.1,1.0};
  GLfloat colorLighGray[] = {0.75,0.75,0.75,1.0};
  GLfloat colorPink[] = {1.0,0.5,0.5,1.0};
static void init(void)  {                            
  //inicializar();
   // glReadBuffer(GL_BACK);                           
   // glClearColor(0.0, 0.0, 0.0, 0.0);                
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);           
    glViewport(0, 0, WIDTH, HEIGHT);                 
   // glMatrixMode(GL_PROJECTION);                     
   // glLoadIdentity();                                
   // glMatrixMode(GL_MODELVIEW);                      
                                                     
// glShadeModel(GL_SMOOTH);
//  glEnable(GL_LIGHTING);
//	glEnable(GL_LIGHT0);
//
//	glLightfv(GL_LIGHT0,GL_POSITION,lighPosition);
//	glLightfv(GL_LIGHT0,GL_DIFFUSE,colorWhite);
//
//	glLightfv(GL_LIGHT0,GL_AMBIENT,colorDarkGray);
//	glLightfv(GL_LIGHT0,GL_DIFFUSE,colorLighGray);
//	glLightfv(GL_LIGHT0,GL_SPECULAR,colorWhite);
//  glMaterialfv(GL_FRONT, GL_DIFFUSE,colorPink );
//  glMaterialf(GL_FRONT, GL_SHININESS,10.0);

    pixels = malloc(FORMAT_NBYTES * WIDTH * HEIGHT); 
  //  time = glutGet(GLUT_ELAPSED_TIME);               
}                                                    


void redisplay_all(void)
{
   // glutSetWindow(window);
 //   screen_reshape(sub_width, sub_height);
		glutTimerFunc(ms,mouse,1);
    glutPostRedisplay();

}

void screen_reshape(GLuint  width, GLuint height)
{

//	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(60.0,1.0,1.0,10.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glClearColor(0.2, 0.2, 0.2, 0.0);
	glEnable(GL_DEPTH_TEST);

}

void mouse() {

				if( nscreenshots < 440 )  //  760 euler angles
				//if( nscreenshots < 4 )  //  760 euler angles
				{
					create_ppm(offFileName, nscreenshots, WIDTH, HEIGHT, 255, FORMAT_NBYTES, pixels);
					//create_pgm(offFileName, nscreenshots, WIDTH, HEIGHT, 255, FORMAT_NBYTES, pixels);
					//csv_gray_scale_pgm(offFileName, nscreenshots, WIDTH, HEIGHT, 255, FORMAT_NBYTES, pixels);
					nscreenshots++;
				}
				else
					exit(1);

				redisplay_all();
}


static void create_ppm(char *prefix, int frame_id, unsigned int width, unsigned int height,
        unsigned int color_max, unsigned int pixel_nbytes, GLubyte *pixels) {
    size_t i, j, k, cur;
    enum Constants { max_filename = 256 };
    char filename[max_filename];
    //snprintf(filename, max_filename, "%s/%s_%f_%f_%f.ppm",dir,prefix,cyl.r,cyl.beta,cyl.z);
    //snprintf(filename, max_filename,"%s/%s_%f_%f_%f.ppm",dir,prefix,sphe.roo,sphe.theta*180/M_PI,sphe.phi*180/M_PI);

    snprintf(filename, max_filename,"%s/%s_%d_%3.3f_%3.3f.ppm",
						dir,prefix,frame_id,fmod(sphe.phi*180/M_PI,360),sphe.theta*180/M_PI);  // ESFERICO
    
		//snprintf(filename,max_filename,"%s/%s_%d_%d_%d_%d.ppm",
		//			dir,prefix,frame_id,euler.phi,euler.tetha,euler.trinche); // EULER
		
    FILE *f = fopen(filename, "w");
    fprintf(f, "P3\n%d %d\n%d\n", width, HEIGHT, 255);
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            cur = pixel_nbytes * ((height - i - 1) * width + j);
            fprintf(f, "%3d %3d %3d ", pixels[cur], pixels[cur + 1], pixels[cur + 2]);
        }
        fprintf(f, "\n");
    }
    fclose(f);
}

static void create_pgm(char *prefix, int frame_id, unsigned int width, unsigned int height,
        unsigned int color_max, unsigned int pixel_nbytes, GLubyte *pixels) {
    size_t i, j, k, cur;
    enum Constants { max_filename = 256 };
    char filename[max_filename];
    //snprintf(filename, max_filename, "%s/%s_%f_%f_%f.ppm",dir,prefix,cyl.r,cyl.beta,cyl.z);
    //snprintf(filename, max_filename,"%s/%s_%f_%f_%f.ppm",dir,prefix,sphe.roo,sphe.theta*180/M_PI,sphe.phi*180/M_PI);

    //snprintf(filename, max_filename,"%s/%s_%d_%3.3f_%3.3f.pgm",
		//				dir,prefix,frame_id,fmod(sphe.phi*180/M_PI,360),sphe.theta*180/M_PI);  // ESFERICO

		snprintf(filename,max_filename,"%s/%s_%d_%d_%d_%d.ppm",
					dir,prefix,frame_id,euler.phi,euler.tetha,euler.trinche); // EULER
    FILE *f = fopen(filename, "w");

    fprintf(f, "P2\n%d %d\n%d\n", width, HEIGHT,255);
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            cur = pixel_nbytes * ((height - i - 1) * width + j);
            //fprintf(f, "%3d %3d %3d ", pixels[cur], pixels[cur + 1], pixels[cur + 2]);
            fprintf(f, "%2d ", (pixels[cur]+pixels[cur + 1]+pixels[cur + 2])/3);
        }
        fprintf(f, "\n");
    }
    fclose(f);
}
static void csv_gray_scale_pgm(char *prefix, int frame_id, unsigned int width, unsigned int height,
        unsigned int color_max, unsigned int pixel_nbytes, GLubyte *pixels) {
    size_t i, j, k, cur;
    enum Constants { max_filename = 256 };
    char filename[max_filename];

    snprintf(filename,max_filename,"%s/%s_%d_%3.3f_%3.3f.csv",
						dir,prefix,frame_id,fmod(sphe.phi*180/M_PI,360),sphe.theta*180/M_PI); 

    FILE *f = fopen(filename, "w");
		fprintf(f, "%d,%d,%d,%d,",objType,euler.phi,euler.tetha,euler.trinche); //EULER
		//fprintf(f, "%d,%3.3f,%3.3f,",objType,fmod(sphe.phi*180/M_PI,360),sphe.theta*180/M_PI); //ESFERICO
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            cur = pixel_nbytes * ((height - i - 1) * width + j);
            //fprintf(f, "%3d %3d %3d ", pixels[cur], pixels[cur + 1], pixels[cur + 2]);
						if ( j == width-1) 
							fprintf(f, "%d",(pixels[cur]+pixels[cur + 1]+pixels[cur + 2])/3);
						else   
							fprintf(f, "%d ",(pixels[cur]+pixels[cur + 1]+pixels[cur + 2])/3);
        }
				if ( i < height-1)
					fprintf(f, " ");
    }
    fclose(f);
}

void vertexByPos( int pos )
{

	glVertex3f(off->axis[pos].x, off->axis[pos].y, off->axis[pos].z);

}

void draw_axes(void)
{

glPushMatrix();
	glColor3f(0.0f, 0.0f, 0.0f); 
	glRasterPos3f(15,0,0);
	glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18,'X');

	glRasterPos3f(0,15,0);
	glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18,'Y');

	glRasterPos3f(0,0,15);
	glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18,'Z');
glPopMatrix(); 

}
void inicializar() {
  GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
GLfloat mat_shininess[] = { 50.0 };
GLfloat light_position[] = { 0.0, 1.0, 1.0, 0.0 };
//glClearColor (0.0, 0.0, 0.0, 0.0);
	glClearColor(1.0f,1.0f,1.0f,1.0);  // Color de fondo: negro
glShadeModel (GL_SMOOTH);

glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
glLightfv(GL_LIGHT0, GL_POSITION, light_position);

glEnable(GL_LIGHTING);
glEnable(GL_LIGHT0);
glEnable(GL_DEPTH_TEST);

}

void display(void)
{
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(1.0f,1.0f,1.0f,1.0);  // Color de fondo: negro
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);  // Boramos la pantalla
  glMatrixMode(GL_PROJECTION); // Modo proyección
  glPushMatrix(); // Guardar la amtriz de proyeccion
  glLoadIdentity(); // Cargamos la matriz identidad   P = I

	// Proyección perspectiva. El ángulo de visualización es de 60
	//grados, la razón ancho/alto es 1 (son inguales), la distancia
	//mínima es z=1.0, y la distancia máxima es z=100.0
	gluPerspective(60.0,1,1,100);
  glPushMatrix(); // Guardar la amtriz del modelo 
  glMatrixMode(GL_MODELVIEW); // Modo proyección
  glLoadIdentity(); // Cargamos la matriz identidad   M = I



  //>>>>>>>>
 //>>>>>>>>>>>


//  glEnable(GL_LIGHTING);
//	glEnable(GL_LIGHT0);
//	GLfloat white[] = {1.0f, 1.0f, 1.0f, 1.0f};
//glLightfv(GL_LIGHT0,GL_POSITION,white);

//glTranslatef(0.333778,0.277507,0.423717);
//---------------------- > SPHERICAL ANGULES START
		spheCoor(nscreenshots);
		gluLookAt(sphe.roo*sin(sphe.theta)*cos(-sphe.phi),
							sphe.roo*sin(sphe.theta)*sin(-sphe.phi),
							sphe.roo*cos(sphe.theta),0,0,0,0,0,1); // Colocamos la camara en el punto deseado
//---------------------- > SPHERICAL ANGULES END



//----------------->> ROTATION START
//	eulerRot(nscreenshots);
//	gluLookAt(0.00001,0.00001,40,0,0,0,0,0,0.2 );
//	glRotatef(euler.phi+45,0,0,1);
//	glRotatef(euler.tetha,1,0,0);
//	glRotatef(euler.trinche,0,0,1);
//	printf("euler.phi = %d\n",euler.phi);
//	printf("euler.tetha = %d\n",euler.tetha);
//	printf("euler.trinche = %d\n",euler.trinche);
//	printf("=============================\n");
//----------------->> ROTATION END 


//if( nscreenshots == 0)
//	glRotatef(45,0,0,1);
//else if( nscreenshots == 1)
//	glRotatef(45+20,0,0,1);
//else if( nscreenshots == 2 ){ 
//	glRotatef(45+20,0,0,1);
//	glRotatef(20,1,0,0);
//	}
//else if( nscreenshots == 3 ){ 
//	glRotatef(45+20,0,0,1);
//	glRotatef(20,1,0,0);
//	glRotatef(20,0,0,1);
//}

		


	glEnable( GL_DEPTH_TEST );

//-----------------> axes start
	draw_axes();
	glBegin(GL_LINES);

	glColor3f(1.0f, 0.0f, 0.0f); 
	glVertex3f(-15, 0, 0);
	glVertex3f(15, 0, 0);

	glColor3f(0.0f, 1.0f, 0.0f); 
	glVertex3f(0, -15, 0);
	glVertex3f(0, 20, 0);

	glColor3f(0.0f, 0.0f, 1.0f); 
	glVertex3f(0, 0, -15);
	glVertex3f(0, 0, 20);

	glEnd();
//-----------------> axes end


int row,col;
glScalef(20*0.75/maxAxis ,20*0.75/maxAxis,20*0.75/maxAxis);


// ---------------------_>
//	glColor3f( 0.0f, 1.0f, 1.0f); 
	//glColor3f( 0.9f, 0.91f, 0.98f); 
//glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE); // Default
//glEnable(GL_COLOR_MATERIAL); // Mind OpenGL pitfall #14.
	for (  row = 0; row < off->numOf.faces; row++ )
	{
		glBegin(GL_POLYGON);
    glColor3f(0.5*(float)off->numOf.faces/(off->numOf.faces + row),
       0.9*(float)off->numOf.faces/(off->numOf.faces + row),
       0.9*(float)off->numOf.faces/(off->numOf.faces + row) ); 
    glNormal3f(0,1,0);
		for ( col = 0; col < off->seq[row].numberOfVertexs; col++ ) 
		{
				vertexByPos(off->seq[row].vertex[col]);
		}
		glEnd();
	}
// --------------------- >


	glColor3f( 0.1f, 0.1f, 0.1f); 
	for (  row = 0; row < off->numOf.faces; row++ )
	{
		glBegin(GL_LINE_LOOP);
		for ( col = 0; col < off->seq[row].numberOfVertexs; col++ ) 
		{
				vertexByPos(off->seq[row].vertex[col]);
		}
		glEnd();
	}
	glutSwapBuffers();
	glReadPixels(0, 0, WIDTH, HEIGHT, FORMAT, GL_UNSIGNED_BYTE, pixels);
//	
}

t_cylCoor coordinatates( int step )
{
	cyl.r= 30;
	cyl.beta= step*8*(M_PI/180) + M_PI;
	cyl.z= 30;
	return cyl;
}


t_spheCorr spheCoor(int step )
{
	int step2 = step/(40);
	sphe.phi  = step*9*(M_PI/180) ;
	sphe.theta =  step2*18*(M_PI/180)+0.0001;
	sphe.roo	= 40;
	return sphe;
}


void eulerRot( int step )
{
	
	//α,β,γ) → (α+π,−β,γ+π)

	// Alfa  : 0,36,72...324 = #10	
	euler.phi 		= (step*36)%360;

	// Betha : 36,72,108,144 = # 4
	if( step%10  == 0 && step > 0){
		betha++;	
		euler.tetha = ((betha*36)%180 == 0) ? 36 : (betha*36)%180;
	}else
		euler.tetha = (betha == 0) ? 0 :((betha*36)%180 == 0) ? 36 : (betha*36)%180;

		
	// Trinche : 0,36,72...324 = #10
	if( step%50  == 0 && step > 0){
		trinche++;
		euler.trinche = (trinche*36)%360;
	}else
		euler.trinche = (trinche*36)%360;
		
}

void usage( char *bin )
{
	printf("./%s -f <offFIle.off> -t [pgm|ppm|csv]",bin);
	exit(-1);
}

int main( int argc, char *argv[])
{	
	size_t len=0;	
	int mk=0;
	char *dirName=NULL;
	if ( argc < 1 )	
	{
		fprintf(stderr,"argc = %d\n",argc);
		exit(-1);
	}

	len = strlen(basename(argv[1]))+1;	
	offFileName =(char *)calloc(len,sizeof(char));
	if ( offFileName == NULL )
		exit(-1);
	
	objType = atoi(argv[2]);
	
	strncpy(offFileName,basename(argv[1]),len);
	printf("file [%s]\n",offFileName);


	dirName =(char *)calloc(225,sizeof(char));
	if ( dirName == NULL )
		exit(-1);
	getcwd(dirName,255);
	printf("dir [%s]\n",dirName);


	dir =(char *)calloc(225,sizeof(char));
	if ( dir == NULL )
		exit(-1);
	snprintf(dir,225,"%s/%s",dirName,offFileName);
	printf("newdir=%s\n",dir);
	mk = mkdir(dir,S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	if ( mk !=0)
	{
		fprintf(stderr,"ERR : [%s]\n",strerror(errno));
		exit(-1);
	}
	off = (t_off *)malloc(sizeof(t_off));
	offFileProcess(off,argv[1]);

//	printf("Min (x,y,z)=(%f,%f,%f)\n",min.x,min.y,min.z);
	maxAxis = maxAxisValue(off->axis,off->numOf.vertexs);

	printf("max [%f]\n",maxAxis);

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGBA | GLUT_DEPTH);
  glutInitWindowPosition(20,20);
  glutInitWindowSize(WIDTH,HEIGHT);
  window = glutCreateWindow(argv[0]);
  init();
	glutTimerFunc(ms,mouse,1);
  glutDisplayFunc(display);
//	glutMouseFunc(mouse);
  glutMainLoop();
	return 0;
}
