
#define _GNU_SOURCE 
#include <stdio.h>  
#include <stdlib.h> 
#include <stdbool.h>
#include <string.h> 

typedef struct seqOfFaces
{ 
  int numberOfVertexs;
  int *vertex;
}t_seq;

typedef struct axis
{ 
  float x;
  float y;
  float z;
}t_axis;

typedef struct numberOf
{ 
  int vertexs;
  int faces;
  int edges;
}t_numOf;

typedef struct off
{ 
  t_numOf numOf;
  t_axis *axis;
  t_seq *seq;

}t_off;

//void offFileProcess( t_off *ptr_off , const char * offFile );
void offFileProcess( t_off *off , const char * offFile );

t_axis maxValue( t_axis *value, int total );
t_axis minValue( t_axis *value, int total );
float maxAxisValue(t_axis *off,int totalVertex );


