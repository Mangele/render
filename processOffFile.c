/*
 *  Miguel Retamozo
 */
#include "processOffFile.h"
/*
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

typedef struct seqOfFaces
{
	int numberOfVertexs;
	int *vertex;
}t_seq;

typedef struct axis
{
	float x;
	float y;
	float z;
}t_axis;

typedef struct numberOf
{
	int vertexs;
	int faces;
	int edges;
}t_numOf;

typedef struct off
{
	t_numOf numOf;
	t_axis *axis;
	t_seq	*seq;

}t_off;

void offFileProcess( t_off off , const char * offFile );

int
main(int argc, char *argv[])
{
	t_off off;
	offFileProcess(off,argv[1]);
	
	 t_off off;
	 FILE *fp =NULL;
	 char *line = NULL;
	 size_t len = 0;
	 ssize_t nread;
	 char *token =NULL;


	 fp = fopen("chair_0175.off", "r");
	 if (fp == NULL) {
			 perror("fopen");
			 exit(EXIT_FAILURE);
	 }

	 while ( (nread = getline(&line, &len, fp)) != -1) 
	 {
			if ( line[0] == '\n' || line[0] == '#' || line[0] == 'O' )
				continue;
			else
				break;
	 }

	 line[nread-1]='\0';
	 token = strsep(&line," ");
	 off.numOf.vertexs = atoi(token);
	 token = strsep(&line," ");
	 off.numOf.faces   = atoi(token);
	 token = strsep(&line," ");
	 off.numOf.edges   = atoi(token);

	 printf("%d %d %d\n",off.numOf.vertexs,off.numOf.faces,off.numOf.edges);
	 int i =0;
	 off.axis = ( t_axis *)calloc(off.numOf.vertexs,sizeof(t_axis));
	 while ( i < off.numOf.vertexs ) 
	 {
		 	nread = getline(&line, &len, fp);
			line[nread-1]='\0';
		 	token = strsep(&line," "); 
			off.axis[i].x =	atol(token);;
			token = strsep(&line," "); 
			off.axis[i].y =	atol(token);;
			token = strsep(&line," "); 
			off.axis[i].z =	atol(token);;
			printf("(x,y,z)=(%.2f,%.2f,%.2f)\n",off.axis[i].x,off.axis[i].y,off.axis[i].z);
			i++;
	 }
	 i=0;
	 int j =0;
	 off.seq = ( t_seq *)calloc(off.numOf.faces,sizeof(t_seq));
	
	 while( i < off.numOf.faces)
	 {
		 	nread = getline(&line, &len, fp);
			line[nread-1]='\0';
					
			token = strsep(&line," "); 
			off.seq[i].numberOfVertexs =	atoi(token);
			printf("[%d]",off.seq[i].numberOfVertexs);
			
			off.seq[i].vertex = ( int *)calloc(off.seq[i].numberOfVertexs,sizeof(int));
			while( j < off.seq[i].numberOfVertexs ){	
				token = strsep(&line," "); 
				off.seq[i].vertex[j] = atoi(token);
				printf("->%d",off.seq[i].vertex[j]);
				j++;
			}
			printf("\n");
			j=0;
			i++;
	 } 
	 fclose(fp);
	return 0;
}
	*/


t_axis maxValue( t_axis *value, int total )
{
	t_axis maxValue;
	int i =0;
	maxValue.x =0;
	maxValue.y =0;
	maxValue.z =0;
	while( i < total  )	
	{
		if ( value[i].x > maxValue.x )
			maxValue.x = value[i].x;	

		if ( value[i].y > maxValue.y )
			maxValue.y = value[i].y;	

		if ( value[i].z > maxValue.z )
			maxValue.z = value[i].z;	
			
		i++;
	}

	return maxValue;
}

t_axis minValue( t_axis *value, int total )
{
	t_axis minValue;
	int i =0;
	minValue.x =0;
	minValue.y =0;
	minValue.z =0;
	while( i < total  )	
	{
		if ( value[i].x < minValue.x )
			minValue.x = value[i].x;	

		if ( value[i].y < minValue.y )
			minValue.y = value[i].y;	

		if ( value[i].z < minValue.z )
			minValue.z = value[i].z;	
			
		i++;
	}

	return minValue;
}

//t_axis eulerRotation(t_axis axis){
//	
//	axis.x= axis.x*( cos(euler.trinche)*cos(euler.phi) - 
//									 cos(euler.theta)*sin(euler.phi)*sin(euler.trinche)) +
//					axis.y*( cos(euler.trinche)*sin(euler.phi) + 
//									 cos(euler.theta)*cos(euler.phi)*sin(euler.trinche))  +
//					axis.z*( sin(euler.theta)*sin(euler.trinche)) ;
//
//	axis.y= axis.x*( -sin(euler.trinche)*cos(euler.phi) -
//										cos(euler.theta)*sin(euler.phi)*cos(euler.trinche))+
//					axis.y*( -sin(euler.trinche)*sin(euler.phi) +
//										cos(euler.theta)*cos(euler.phi)*cos(euler.trinche))+
//					axis.z*( sin(euler.theta)*cos(euler.phi));
//
//	axis.z= axis.x*( sin(euler.theta)*sin(euler.phi))  -
//					axis.y*( sin(euler*thetat)*cos(euler.phi)) +
//					axis.z*( cos(euler.theta));
//
//	return axis;
//}

float maxAxisValue(t_axis *axis,int totalVertex )
{

	int i=0;
	float value=0;
	float max[3]	= {0};
	t_axis maxXYZ = maxValue(axis,totalVertex);
//	t_axis minXYZ = minValue(axis,totalVertex);
	//t_axis minXYZ = minValue(off->axis,off->numOf.vertexs)
	printf("Max (x,y,z)=(%f,%f,%f)\n",maxXYZ.x,maxXYZ.y,maxXYZ.z);
//	printf("Min (x,y,z)=(%f,%f,%f)\n",minXYZ.x,minXYZ.y,minXYZ.z);

	//max[0]=maxXYZ.x-minXYZ.x;
	//max[1]=maxXYZ.y-minXYZ.y;
	//max[2]=maxXYZ.z-minXYZ.z;

	max[0]=maxXYZ.x;
	max[1]=maxXYZ.y;
	max[2]=maxXYZ.z;
	while(i < 3 ) 
	{
		if ( max[i] > value )
			value = max[i];
		i++;
	}

	return value;
}



void offFileProcess( t_off *off , const char * offFile )
{

//t_off off;     
//ptr_off = &off;


	 FILE *fp =NULL;
	 char *line = NULL;
	 size_t len = 0;
	 ssize_t nread;
	 char *token =NULL;

	 fp = fopen(offFile, "r");
	 if (fp == NULL) {
			 perror("fopen");
			 exit(EXIT_FAILURE);
	 }

	 while ( (nread = getline(&line, &len, fp)) != -1) 
	 {
			if ( line[0] == '\n' || line[0] == '#' || line[0] == 'O' )
				continue;
			else
				break;
	 }

	 if( line[nread-1] == 0x20 ){
   	line[nread-2]='\0';
		printf("Space\n");
	 }

	 line[nread-1]='\0';
	 token = strsep(&line," ");
	 off->numOf.vertexs = atoi(token);
	 token = strsep(&line," ");
	 off->numOf.faces   = atoi(token);
	 token = strsep(&line," ");
	 off->numOf.edges   = atoi(token);

	 //printf("%d %d %d\n",off->numOf.vertexs,off->numOf.faces,off->numOf.edges);
	 int i =0;
	 off->axis = ( t_axis *)calloc(off->numOf.vertexs,sizeof(t_axis));
	 while ( i < off->numOf.vertexs ) 
	 {
		 	nread = getline(&line, &len, fp);
			if( line[nread-2] == 0x20 )
				line[nread-2]='\0';
			line[nread-1]='\0';
		 	token = strsep(&line," "); 
			off->axis[i].x =	atof(token);;
			token = strsep(&line," "); 
			off->axis[i].y =	atof(token);;
			token = strsep(&line," "); 
			off->axis[i].z =	atof(token);;
		//	printf("(x,y,z)=(%f,%f,%f)\n",off->axis[i].x,off->axis[i].y,off->axis[i].z);
			i++;
	 }
	 i=0;
	 int j =0;
	 off->seq = ( t_seq *)calloc(off->numOf.faces,sizeof(t_seq));
	
	 while( i < off->numOf.faces)
	 {
		 	nread = getline(&line, &len, fp);
			if( line[nread-2] == 0x20 )
				line[nread-2]='\0';
				
			line[nread-1]='\0';
					
			token = strsep(&line," "); 
			off->seq[i].numberOfVertexs =	atoi(token);
			//printf("[%d]",off->seq[i].numberOfVertexs);
			
			off->seq[i].vertex = ( int *)calloc(off->seq[i].numberOfVertexs,sizeof(int));
			while( j < off->seq[i].numberOfVertexs ){	
				token = strsep(&line," "); 
				off->seq[i].vertex[j] = atoi(token);
			//	printf("->%d",off->seq[i].vertex[j]);
				j++;
			}
			//printf("\n");
			j=0;
			i++;
	 }

	 fclose(fp);

}
